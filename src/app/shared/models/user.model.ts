export class User {
  constructor(
    public id: number,
    public googleId: string,
    public username: string,
    public usernameSise: string,
    public nome: string,
    public firstName: string,
    public lastName: string,
    public email: string,
    public picture: string,
    public permissoes: Permissao,
  ) {}
}

export class Permissao {
  constructor(
    public id: number,
    public tipoPermissao: string,
    public status: string,
  ) {}
}

export class TipoPermissao {
  constructor(
    public id: number,
    public sistema: Sistema,
    public nivel: number,
    public descricao: string,
  ) {
  }
}

export class Sistema {
  constructor(
    public id: number,
  ) {
  }
}
