import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../models/user.model';
import {INTRANET_API} from '../../app.api';
import {AuthService, GoogleLoginProvider, SocialUser} from 'angularx-social-login';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  user: User

  constructor(private http: HttpClient,
              private authService: AuthService,
              private router: Router) {}

  /**
   * user request
   * @param socialUser
   */
  loadUser(socialUser: SocialUser): Observable<User> {
    const usernameSise = socialUser.email.replace('@unicamp.br', '')
    console.log(`loading user ${usernameSise}`)
    let params: HttpParams
    params = new HttpParams()
      .append('group', 'show')
      .append('usernameSise', usernameSise)
      .append('nome', socialUser.name)
      .append('googleId', socialUser.id)
      .append('picture', socialUser.photoUrl)
      .append('firstName', socialUser.firstName)
      .append('lastName', socialUser.lastName)
      .append('email', socialUser.email)
    return this.http.get<User>(`${INTRANET_API}/user/getUser`, {params: params})
  }

  /**
   * Logouts user
   */
  logOut() {
    console.log('loggin out user')
    this.authService.signOut(true)
    this.user = null
    this.router.navigateByUrl(`/`)
  }

  loginGoogle() {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
  }

  /**
   *
   */
  isLogged(): boolean {
    if (this.user) {
      return true
    } else {
      return false
    }
  }

  /**
   *
   */
  getUser(): Observable<User> {
    return new Observable( (observer) => {
      if (!this.user) {
        this.authService.authState.subscribe( socialUser => {
          // check if user is logged
          if (!socialUser) {
            observer.next(null)
          } else {
            this.loadUser(socialUser).subscribe( (user) => {
              this.user = user
              observer.next(this.user)
            })
          }
        })
      } else {
        observer.next(this.user)
      }
    })
  }
}
