import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './page/index/index.component';



@NgModule({
  declarations: [IndexComponent],
  imports: [
    CommonModule
  ]
})
export class AgendaModule { }
